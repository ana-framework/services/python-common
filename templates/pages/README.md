# Usage

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  - component: gitlab.com/ana-framework/services/pages@1.0.0
```

Use as a CI/CD component
Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/ana-framework/services/pages@1.0.1
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      pagesStage: documentation
```


Use as a CI/CD template (legacy)
Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'ana-framework/services'
    ref: '1.0.1'
    file: '/templates/pages.yml'

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
```


## Configuration

Thanks to the Gitlab `inputs` system you can configure your implemented Components. 

### Parameters

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `pagesStage` | Pages stage. | `'documentation'` |

