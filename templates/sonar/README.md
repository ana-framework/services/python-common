# Usage

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
  - component: gitlab.com/ana-framework/services/sonar@1.0.0
```

Use as a CI/CD component
Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/ana-framework/services/sonar@1.0.1
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      sonarStage: test
      sonarToken: 7e753f16e0e8a47d24c25f82050946d05e23724f
```


Use as a CI/CD template (legacy)
Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'ana-framework/services'
    ref: '1.0.1'
    file: '/templates/sonar.yml'

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
  SONAR_TOKEN: 7e753f16e0e8a47d24c25f82050946d05e23724f
  GIT_DEPTH: 0
```


## Configuration

Thanks to the Gitlab `inputs` system you can configure your implemented Components. 

### Parameters

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `sonarStage` | Sonar Cloud stage. | `'test'` |
| `sonarUserHome` / `SONAR_USER_HOME` | SONAR USER HOME            # Defines the location of the analysis task cache | `'${CI_PROJECT_DIR}/.sonar'` |
| `gitDepth` / `GIT_DEPTH` | GIT DEPTH                  # Tells git to fetch all the branches of the project, required by the analysis task | `'0'` |
| `sonarToken` / `SONAR_TOKEN` | SONAR TOKEN                # It is unic per project | `'7e753f16e0e8a47d24c25f82050946d05e23724f'` |
| `sonarHostUrl` / `SONAR_HOST_URL` | SOnar URL | `'https://sonarcloud.io'` |

