# GitLab CI template for Sonar Cloud

This projects is a Gitlab [CI/CD Components](https://docs.gitlab.com/ee/ci/components/) that's implements [Sonar](https://github.com/GoogleContainerTools/sonar). 

# Usage

## CI/CD Components

Some pipeline configurations have been transitioned to [CI/CD components](https://docs.gitlab.com/ee/ci/components/):

- [`pages` component](templates/pages)
- [`sonar` component](templates/sonar)

